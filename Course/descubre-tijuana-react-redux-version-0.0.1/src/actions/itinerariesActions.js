"use strict"
export function getItineraries(data) {
    return {
        type: "GET_ITINERARIES",
        payload: data
    }
}