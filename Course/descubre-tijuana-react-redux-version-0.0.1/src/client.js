"use strict"
import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';

// Router
import {Router, Route, IndexRoute, browserHistory} from 'react-router';

import {applyMiddleware, createStore} from 'redux';
// import logger from 'redux-logger';
import thunk from 'redux-thunk';

import reducers from './reducers/index';

const middleware = applyMiddleware(thunk);
const store = createStore(reducers);

import ItinerariesList from './components/itinerariesList';
import Main from './main';

const Routes = (
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path="/" component={Main}>
            <IndexRoute component={ItinerariesList} />
            </Route>
        </Router>
    </Provider>
)

render(
    Routes, document.getElementById('app')
);