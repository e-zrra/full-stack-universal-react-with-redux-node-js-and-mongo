"use strict"
import React from 'react';
import { Nav, NavItem, Navbar } from 'react-bootstrap';

class Header extends React.Component {
    render() {
        return (
            <Navbar inverse fixedTop>
            <Navbar.Header>
              <Navbar.Brand>
                <a href="/">ITINERARIOS</a>
              </Navbar.Brand>
              <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
              <Nav>
                <NavItem eventKey={1} href="/">ITINERARIOS</NavItem>
              </Nav>
            </Navbar.Collapse>
            </Navbar>
        )
    }
}

export default Header