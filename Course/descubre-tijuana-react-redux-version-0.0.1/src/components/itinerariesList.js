"use strict";

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getItineraries } from '../actions/itinerariesActions';

class ItinerariesList extends React.Component {

    componentDidMount() {
        this.props.getItineraries();
        console.log(this.props.itineraries)
    }

    render() {
        
        return (
            <div>
                
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        itineraries: state.itineraries
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getItineraries: getItineraries
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ItinerariesList);