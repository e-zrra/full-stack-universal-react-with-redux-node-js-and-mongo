"use strict"
import React from 'react';
import { Grid, Col, Row, Glyphicon } from 'react-bootstrap';
class Menu extends React.Component {
    render() {
        const styleContent = {
            position: "fixed",
            width: "100%",
            bottom: "0",
            overflow: "hidden"
        }

        const styleCol = {
            textAlign: "center",
            padding: "20px",
            background: "gray"
        }

        const styleGlyph = {
            color: "#fff",
            fontSize: "22px"
        }

        return (
            <div className="menu" style={styleContent}>
                <Grid>
                    <Row>
                        <Col xs={4} style={styleCol}>
                            <Glyphicon style={styleGlyph} glyph="th-list" />
                        </Col>
                        <Col xs={4} style={styleCol}>
                            <Glyphicon style={styleGlyph} glyph="map-marker" />
                        </Col>
                        <Col xs={4} style={styleCol}>
                            <Glyphicon style={styleGlyph} glyph="search" />
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}

export default Menu