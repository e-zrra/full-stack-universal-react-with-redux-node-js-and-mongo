"use strict";
import React from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Header from './components/header';
import Menu from './components/menu';

class Main extends React.Component {
    componentDidMount() {
        
    }
    
    render() {
        const styleContent = {
            marginTop: "50px"
        }

        return (
            <div>
                <Header />
                <div style={styleContent}>
                    {this.props.children}
                </div>
                <Menu />
            </div>
        );
    }
}

function mapStateToProps(state){
    return {

    }
}

export default Main;