"use strict";
import {combineReducers} from 'redux';

import {itinerariesReducers} from './itinerariesReducers';

export default combineReducers({
    itineraries: itinerariesReducers
});