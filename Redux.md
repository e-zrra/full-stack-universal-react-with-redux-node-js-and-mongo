Application State

Is a state container where you can store your application state in an efficient and convenient manner.

React-Redux
Store
Provider
Actions
Reducers
Middleware (can be optional in certain scenarios)

React App
Smart Components (Also called Containers, Application state)
-> 
Pass data as propos
Dum Components (Also called Presentation Components)

How react-redux work together

1 Store -> Application state (this.state.A, this.state.B, etc)

2 Provider (SMART COMPONENTS)
render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('app')
)

3. Actions
Actions Creator

5 Middlewares
Are optional

4 Reducers

---> got to App state 1



### Three principles of redux

1. Single source of thruth
The state of your whole application is stored in an object tree within a single store

2. State is read-only
the only way to change the state is to emit an Action

3. changes are made with a pure functions:
Reducers have to be pure-functions


### IMMUTABILITY of the state
1. When making operations with Arrays:
Do not use mutable methods: push() or splice()

USE: concat(), slice() or spread operator.

2. When making operations with Objects
USE: Object.Assign() or spread operator