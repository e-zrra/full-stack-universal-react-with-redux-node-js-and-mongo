Install Webpack global & Dev dependencies
```
npm i webpack -g
npm i --save-dev webpack
```

Install Express
```
npm i --save express
```

Install others dependencies
```
npm i --save babel-core babel-loader babel-preset-es2015 babel-preset-stage-1 babel-preset-react
```

Install Redux
```
npm i --save redux
```