import React from "react";
import {connect} from "react-redux";
import { push } from "react-router-redux";
import {Nav, NavItem, Navbar, Badge} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';

let NavigationComponent = (props) => (
    <Navbar inverse fixedTop>
    <Navbar.Header>
      <Navbar.Brand>
        <a href="/">React-Bootstrap</a>
      </Navbar.Brand>
      <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
      <Nav>
        <LinkContainer to="/about">
          <NavItem eventKey={1} href="/about">About</NavItem>
        </LinkContainer>
        <NavItem eventKey={2} href="/contacts">Contact Us</NavItem>
      </Nav>
      <Nav pullRight>
        <NavItem eventKey={1} href="/admin">Admin</NavItem>
        <NavItem eventKey={2} href="/cart">Your Cart
          {(props.cartItemsNumber > 0)?(<Badge className="badge">{props.cartItemsNumber}</Badge>):('')}
        </NavItem>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
)

const state = (state, ownProps = {}) => {
    return {
        location: state.location
    }
}

const mapDispatchToProps = (dispatch, ownProps) => ({
    navigateTo: (location) => {
        dispatch(push(location));
    }
});

export default connect(state, mapDispatchToProps)(NavigationComponent);