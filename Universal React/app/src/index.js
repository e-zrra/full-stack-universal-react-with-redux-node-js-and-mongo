"use strict"
//React
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

// Logger & middleware
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

// Import combined reducers
import reducers from './reducers/index';

// Router
import {BrowserRouter} from 'react-router-dom';
//import { ConnectedRouter as Router, routerReducer, routerMiddleware } from 'react-router-redux';
//import createHistory from 'history/createBrowserHistory';

// IMPORT ACTIONS
import {addToCart} from './actions/cartActions';

//const history = createHistory();
//const middlewareRouter = routerMiddleware(history);

// store & middleware
const middleware = applyMiddleware(thunk, logger);
// WE WILL PASS INITIAL STATE FROM SERVER STORE
const initialState = window.INITIAL_STATE;
const store = createStore(reducers, initialState, middleware);

// Components
import App from './app';

import routes from './routes';
const Routes = (
    <Provider store={store}>
        <BrowserRouter>
            {routes}
        </BrowserRouter>
    </Provider>
)

render(
    Routes, document.getElementById('app')
);