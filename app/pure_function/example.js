// PURE FUNCTION
// Given the same input, it will always return the same result
function checkAge(age) {
    let minimun = 18;
    return age >= minimun // FALSE
}

// IMPURE FUNCTION
// Given the same input, we can't be sure the result it will not change
var minimun = 15;
function checkAge(age) {
    return age >= minimun // It depends what minimum value is
}

checkAge(16);