store.subscribe(function () {
  console.log("current state is: ", store.getState());
  console.log("current price: ", store.getState()[1].price);  
});

// create and dispatch actions

store.dispatch({type: "INCREMENT", payload: 1})
store.dispatch({type: "INCREMENT", payload: 1})
store.dispatch({type: "DECREMENT", payload: 1})

store.dispatch(postBooks(
    [{
        id: 1,
        title: 'this is the book title',
        description: 'text',
        price: 33.33
    },
    {
        id: 2,
        title: 'this is the book title',
        description: 'text',
        price: 50
    }]
));

// DISPATCH a second action
// DELETE a book
store.dispatch(deleteBooks(
    {
        id: 1
    }
));

// UPDATE a book
 
store.dispatch(updateBooks(
    {
        id: 2,
        title: "Learn React in 24h"
    }
));

store.dispatch({
    type: "UPDATE_BOOK",
    payload: {
        id: 2,
        title: "Learn React & redux"
    }
});

// --> Cart actions
store.dispatch(addToCart([{id: 1}]))