"use strict";

import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import {Grid, Col, Row, Button} from 'react-bootstrap';

import BooksList from './components/pages/booksList';
import About from './components/pages/about';
import BookForm from './components/pages/booksForm';
import Cart from './components/pages/cart';

import Navigation from './components/navigation';
import Footer from './components/footer';
import {connect} from 'react-redux';

import {bindActionCreators} from 'redux';
import {getCart} from '../src/actions/cartActions';

class App extends React.Component {
    componentDidMount(){
        this.props.getCart();
    }
    render() {
        return(
            <div className="App">
                {/* Header Nav */}
                <div className="App-Header">
                    <Navigation cartItemsNumber={this.props.totalQty} context={this.context} />
                </div>
                {/* Route */}
                <div className="App-main">
                    <Grid>
                        <Route exact path="/" component={BooksList} />
                        <Route exact path="/about" component={About} />
                        <Route exact path="/bookForm" component={BookForm} />
                        <Route exact path="/cart" component={Cart} />
                        <Route exact path="/admin" component={BookForm} />
                    </Grid>
                </div>
                {/* Footer */}
                <Footer />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        totalQty: state.cart.totalQty
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getCart: getCart
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App)