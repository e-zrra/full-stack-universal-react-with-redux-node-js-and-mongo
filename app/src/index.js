"use strict"
//React
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

// Logger & middleware
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

// Import combined reducers
import reducers from './reducers/index';

// Import actions
// import { addToCart } from './actions/cartActions';
// import { postBooks, deleteBooks, updateBooks } from './actions/booksActions';

// Router
import { ConnectedRouter as Router, routerReducer, routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';

const history = createHistory();
const middlewareRouter = routerMiddleware(history);

// store & middleware
const middleware = applyMiddleware(thunk, middlewareRouter, logger);
const store = createStore(reducers, middleware);

// Components
import App from './app';

render(
    <Provider store={store}>
        <Router history={history}>
            <div>
                <App />
            </div>
        </Router>
    </Provider>, document.getElementById('app')
);