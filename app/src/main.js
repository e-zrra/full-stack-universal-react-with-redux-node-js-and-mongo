"use strict"
import React from 'react';
import {Link, Route} from 'react-router-dom';
import BooksList from './components/pages/booksList';
import Cart from './components/pages/cart';
import BooksForm from './components/pages/booksForm';
import About from './components/pages/about';

class Main extends React.Component {
    render() {
        return (
            <div>
                 <Menu />
                    {this.props.children}
                 <Footer />
            </div>
        );
    }
}
export default Main