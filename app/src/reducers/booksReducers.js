"use strict"

// BOOK REDUCERS
export function booksReducers (state={books:[]}, action) {
    switch(action.type) {
        case "GET_BOOKS":
            return {...state, books: [...action.payload]}
        break;
        case "POST_BOOK":
        //let books = state.books.concat(action.payload)
        //return {books};
            return {...state, books: [...state.books, ...action.payload], msg:'Saved! Click to continue', 
            style:'success', validation:'success'}
        break;
        case "POST_BOOK_REJECTED":
            return {...state, msg:'Please, try again', style:'danger', validation:'error'}
        break;
        case "RESET_BUTTON":
            return {...state, msg:null, style:'primary', validation:null}
        break;
        case "DELETE_BOOK":
            // Create a copy of the current array of book
            const currentBooktoDelete = [...state.books]
            // Determine at which index in books array is the book to be deleted
            const indexToDelete = currentBooktoDelete.findIndex(
                function (book) {
                    // return book._id.toString() === action.payload;
                    return book._id == action.payload;
                }
            );
            return {books: [...currentBooktoDelete.slice(0, indexToDelete), ...currentBooktoDelete.slice(indexToDelete + 1)]}
        break;
        case "UPDATE_BOOK":
            // Create a copy of the current array of book
            const currentBooktoUpdate = [...state.books]
            // Determinate at which index in books array is the book to ve updated
            const indexToUpdate = currentBooktoUpdate.findIndex(
                function(book) {
                    return book._id === action.payload._id
                }
            )
            // Create a new book object with the new values and with the same array index of the item we want to replace.
            // To achieve this we will use ..spread but we could use concat methods too
            const newBookToUpdate = {
                ...currentBooktoUpdate[indexToUpdate],
                title: action.payload.title
            }

            // Use slice to remove the book at the specified index, replace with the new object and concatenate with he rest of items in the array
            return {books: [...currentBooktoUpdate.slice(0, indexToUpdate), newBookToUpdate, ...currentBooktoUpdate.slice(indexToUpdate + 1)]}
        break;
    }
    return state;
}