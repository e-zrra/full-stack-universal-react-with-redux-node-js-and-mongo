"use strict"

import {combineReducers} from 'redux';
import {booksReducers} from './booksReducers';
import {cartReducers} from './cartReducers';
import {routerReducer} from 'react-router-redux';

export default combineReducers({
    books: booksReducers,
    cart: cartReducers,
    router: routerReducer
})